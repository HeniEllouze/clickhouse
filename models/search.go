package search

import "database/sql"

type Search struct {
	Db *sql.DB
}

///// Select Request
func (search Search) Find() (Client []client.client, err error) {
	rows, err := search.Db.Query("SELECT * FROM client")
	if err != nil {
		return nil, err
	} else {

		var Clients []client.client
		for rows.Next() {
			var ID int64
			var Name string
			var Prenom string
			var Adress string
			var Numero string
			var CIN string
			err2 := rows.Scan(&ID, &Name, &Prenom, &Adress, &Numero, &CIN)

			if err2 != nil {
				return nil, err2
			} else {
				clients := client.client{

					ID:     ID,
					Name:   Name,
					Prenom: Prenom,
					Adress: Adress,
					Numero: Numero,
					CIN:    CIN,
				}
				client = append(Clients, clients)
			}
		}
		return client, nil
	}
}

//insert Request
func (search Search) Create(Client *client.client) (err error) {
	result, err := search.Prepare("INSERT INTO client(ID,Name,Prenom,Adress,Numero,CIN) values(?,?,?,?,?,?)", Client.ID, Client.Name, Client.Prenom, Client.Adress, Client.Numero.Client.CIN)
	if err != nil {
		return err
	} else {

		Client.Id, _ = result.LastInsertId()
		return nil

	}
}

