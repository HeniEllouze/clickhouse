package client

import (
	"gitlab.com/HeniEllouze/clikhouse-api/entities/client"
	"gitlab.com/HeniEllouze/clickhouse-api/config/database"
	"gitlab.com/HeniEllouze/clickhouse-api/models/search"
	"encoding/json"
	"net/http"
	"search"
)

/// Fonction search pour l'affichage
func Find(response http.ResponseWriter, request *http.Request) {
	db, err := database.dbconnect()
	if err != nil {
		return
	} else {
		search := search.Search{
			Db: db,
		}
		client, err2 := search.Find()
		if err2 != nil {
			return nil, err2
		} else {

			foo(response, http.StatusOK, client)

		}
	}
}

///Fonction create pour l'insertion
func Create(response http.ResponseWriter, request *http.Request) {
	var client client.client
	err := json.NewDecoder(request.Body).Decode(&client)
	db, err := database.dbconnect()
	if err != nil {
		respondWithError(response, http.StatusBadRequest, err.Error())
	} else {
		search := search.search{
			Db: db,
		}
		err2 := search.create(&client)

		if err2 != nil {
			respondWithError(response, http.StatusBadRequest, err.Error())
		} else {

			foo(response, http.StatusOK, client)

		}

	}
}

//
func foo(w http.ResponseWriter, code int, payload interface{}) {
	response, _ := json.Marshal(payload)

	w.Write(response)
}
func respondWithError(w http.ResponseWriter, code int, msg string) {
	foo(w, code, map[string]string{"error": msg})
}

