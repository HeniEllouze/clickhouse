package client

import "fmt"

type client struct {

Id int64
Name string
Prenom string
Adress string
Numero string
CIN string
}
func (client client) ToString() string {
       return fmt.Sprint("id: %d\nname: %s\nprenom: %s\nadresse: %s\nnumero :%s\ncin: %s", client.Id, client.Name, client.Prenom, client.Adress, client.Numero, client.CIN)
}
