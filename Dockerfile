FROM golang:latest
WORKDIR clickhouse-api 
COPY . .
RUN go mod download
ENTRYPOINT go run main.go 
