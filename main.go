package main

import (
	"gitlab.com/HeniEllouze/clickhouse-api/client_api"
	"gitlab.com/HeniEllouze/clickhouse-api/database"
	"fmt"
	"log"
	"net/http"

	"github.com/ClickHouse/clickhouse-go"
	"github.com/gorilla/mux"
)

func main() {

	database, err = database.dbconnect()
	if err != nil {
		log.Println("!!! ERROR IN DB : ", err)
	}
        _, err = database.Exec(`
		CREATE TABLE IF NOT EXISTS client (
                 Id int64
		 Name string
		 Prenom string
		 Adress string
		 Numero string
		 CIN string			
		) engine=Memory
	`)

	router := mux.NewRouter()
	router.HandleFunc("/api/client/search", client_api.Find).Methods("GET")
	router.HandleFunc("/api/client/insert", client_api.Create).Methods("POST")
	err := http.ListenAndServe(":5000", router)
	if err != nil {
		fmt.Println(err)
	}

}

