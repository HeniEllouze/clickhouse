package database

import (
	"database/sql"
	"fmt"
)

func dbconnect() (*sql.DB, error) {

	connect, err := sql.Open("clickhouse", "http://default:@146.59.204.94:8123/default?read_timeout=10&write_timeout=20")

	if err != nil {
		return nil, err
	}

	// defer connect.Close()

	fmt.Println("succefully connected")
	return connect, nil
}
