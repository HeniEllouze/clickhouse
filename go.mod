module gitlab.com/HeniEllouze/clickhouse-api

go 1.13

require (
	github.com/ClickHouse/clickhouse-go v1.4.3
	github.com/gorilla/mux v1.8.0
)
